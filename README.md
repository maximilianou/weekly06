# weekly06


Timmy is hanging out at home, and is crazy board.  

The thing to do at the moment is bing watch a bunch of tv shows and movies, but it's so hard to pick what show to watch.  

Lately, Timmy has been spending more time trying to pick something to watch than actually watching something!

Build something that will help them pick something good!

Make an app that can:

1. Accept entries for movies or tv shows that they like to watch

2. pick a show at random to watch

If you want to go the extra mile:

1. Build in a list of shows to watch so people don't have to enter shows in by hand

2. Ask people questions about what they are feeling right now to decide something that fits with their vibe.
